terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_account" "account_info" {}

# Authentication with SSH keys
data "digitalocean_ssh_key" "ssh_keys" {
  for_each = var.ssh_keys
  name     = each.value
}

# Create a Project and assign resources to it
resource "digitalocean_project" "capstone_petclinic" {
  name        = "Capstone Petclinic"
  description = "A capstone project for my DevOps Internship."
  purpose     = "DevOps"
  resources = [
    digitalocean_droplet.capstone_nexus[0].urn,
    digitalocean_droplet.capstone_jenkins[0].urn,
    digitalocean_droplet.capstone_agents[0].urn
  ]
}

# VPC
resource "digitalocean_vpc" "capstone_petclinic_network" {
  name        = "capstone-petclinic-network"
  description = "VPC for the project."
  region      = var.region["default"]
  ip_range    = "10.0.0.0/16"
}

# We declare which droplets will be assigned reserved ips
locals {
  droplets = [
    {
      name        = "capstone_nexus"
      droplet_ref = digitalocean_droplet.capstone_nexus[0]
    },
    {
      name        = "capstone_jenkins"
      droplet_ref = digitalocean_droplet.capstone_jenkins[0]
    },
    {
      name        = "capstone_agents"
      droplet_ref = digitalocean_droplet.capstone_agents[0]
    },
  ]
}

# We assign reserved ips to the droplets in locals
resource "digitalocean_reserved_ip" "capstone_petclinic_reservedip" {
  for_each = { for droplet in local.droplets : droplet.name => droplet }

  droplet_id = each.value.droplet_ref.id
  region     = each.value.droplet_ref.region
}

# Instance Nexus 3
resource "digitalocean_droplet" "capstone_nexus" {
  count    = 1
  image    = "ubuntu-22-04-x64"
  name     = "petclinic-nexus3-${var.region["default"]}-${count.index + 1}"
  region   = var.region["default"]
  size     = var.basic_droplet_sizes["medium-2"]
  ssh_keys = [
    for ssh_key in data.digitalocean_ssh_key.ssh_keys : ssh_key.id
  ]

  vpc_uuid = digitalocean_vpc.capstone_petclinic_network.id

  lifecycle {
    create_before_destroy = true
  }
}

# Instance Jenkins
resource "digitalocean_droplet" "capstone_jenkins" {
  count    = 1
  image    = "ubuntu-22-04-x64"
  name     = "petclinic-jenkins-${var.region["default"]}-${count.index + 1}"
  region   = var.region["default"]
  size     = var.basic_droplet_sizes["medium-1"]
  ssh_keys = [
    for ssh_key in data.digitalocean_ssh_key.ssh_keys : ssh_key.id
  ]

  vpc_uuid = digitalocean_vpc.capstone_petclinic_network.id

  lifecycle {
    create_before_destroy = true
  }
}

# Instance Agents
resource "digitalocean_droplet" "capstone_agents" {
  count    = 1
  image    = "ubuntu-22-04-x64"
  name     = "petclinic-agents-${var.region["default"]}-${count.index + 1}"
  region   = var.region["default"]
  size     = var.basic_droplet_sizes["medium-1"]
  ssh_keys = [
    for ssh_key in data.digitalocean_ssh_key.ssh_keys : ssh_key.id
  ]

  vpc_uuid = digitalocean_vpc.capstone_petclinic_network.id

  lifecycle {
    create_before_destroy = true
  }
}

# Output of the terraform actions
output "droplets_info" {
  value = {
    droplet_limit           = data.digitalocean_account.account_info.droplet_limit
    capstone_nexus_ip       = digitalocean_droplet.capstone_nexus[0].ipv4_address
    capstone_nexus_type     = "Public"
    capstone_jenkins_ip     = digitalocean_droplet.capstone_jenkins[0].ipv4_address
    capstone_jenkins_type   = "Public"
    capstone_agents_ip      = digitalocean_droplet.capstone_agents[0].ipv4_address
    capstone_agents_ip_type = "Public"
    vpc_name                = digitalocean_vpc.capstone_petclinic_network.name
    vpc_ip_range            = digitalocean_vpc.capstone_petclinic_network.ip_range
  }
}
