#!/bin/bash

TOKEN="YOUR_DIGITALOCEAN_API_TOKEN"

# Get the list of Droplets
droplets=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/droplets")

# Extract Droplet IDs from the response
ids=$(echo "$droplets" | jq -r '.droplets[].id')

# Loop through each Droplet and power it off
for id in $ids; do
    echo "Powering off Droplet with ID $id..."
    curl -s -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"type":"power_off"}' "https://api.digitalocean.com/v2/droplets/$id/actions" >/dev/null
done

echo "All Droplets have been powered off."
