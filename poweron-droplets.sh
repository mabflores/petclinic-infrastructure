#!/bin/bash

TOKEN="YOUR_DIGITALOCEAN_API_TOKEN"

# Get the list of powered off Droplets
droplets=$(curl -s -X GET -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" "https://api.digitalocean.com/v2/droplets?per_page=200&status=off")

# Extract Droplet IDs from the response
ids=$(echo "$droplets" | jq -r '.droplets[].id')

# Loop through each powered off Droplet and power it on
for id in $ids; do
    echo "Powering on Droplet with ID $id..."
    curl -s -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -d '{"type":"power_on"}' "https://api.digitalocean.com/v2/droplets/$id/actions" >/dev/null
done

echo "All powered off Droplets have been powered on."
